<!doctype html>
<html>
<head>
  <title>Pendu App : Admin</title>
  <link rel="stylesheet" href="style.css" type='text/css'>
</head>

<body>

  <div class="container">

    <h1>Mots :</h1>
    <div class="section">
      <div class="listing">
        <ul>
          <?php
          $handle = mysqli_connect("localhost","root","mm5-blm", "PenduApp");
          $query = "SELECT * FROM mots";
          $result = mysqli_query($handle,$query);
          while($line = mysqli_fetch_array($result)) {
            echo "\t<li>";
            echo "[" . $line["id"] ."] ";
            echo $line["mot"];
            echo "&nbsp;<a href=delete_mot.php?id=" . $line["id"] . ">x<a/>";
            echo "</li>\n";
          }
          ?>
        </ul>
      </div>
    </div>
    
    <form method="post" action="create_mot.php">
      <input name="mot" type="text" placeholder="nouveau mot">
      <input type = "submit" value="Ajouter">
    </form>

    <p>Modifier un mot :</p>
    <form action="update_mot.php" method="get"><br>
      <input name="id" type="text" placeholder="ID"><br>
      <input name="mot" type="text" placeholder="Nouveau mot"><br>
      <input type="submit" value="Modifier">
    </form>

  </div>

  <div class="container">

    <h1>Joueurs :</h1>
    <div class="section">
      <div class="listing">
        <ul>
          <?php
          $handle = mysqli_connect("localhost","root","mm5-blm", "PenduApp");
          $query = "SELECT * FROM joueurs";
          $result = mysqli_query($handle,$query);
          while($line = mysqli_fetch_array($result)) {
            echo "\t<li>";
            echo "[" . $line["id"] ."] ";
            echo $line["nom"];
            echo "&nbsp;<a href=delete_joueur.php?id=" . $line["id"] . ">x<a/>";
            echo "</li>\n";
          }
          ?>
        </ul>
      </div>
    </div>

    <form method="post" action="create_joueur.php">
      <input name="joueur" type="text" placeholder="Nouveau joueur">
      <input type = "submit" value="Ajouter">
    </form>

    <p>Modifier un joueur :</p>
    <form action="update_joueur.php" method="get"><br>
      <input name="id" type="text" placeholder="ID"><br>
      <input name="nom" type="text" placeholder="Nouveau nom"><br>
      <input type="submit" value="Modifier">
    </form>

  </div>

</body>
</html>
